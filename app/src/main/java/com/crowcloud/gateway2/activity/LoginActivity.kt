package com.crowcloud.gateway2.activity

import android.content.Intent
import android.os.Bundle
import androidx.navigation.findNavController
import com.crowcloud.gateway.suricataCore.activity.BaseActivity
import com.crowcloud.gateway.suricataCore.events.EventLoginDone
import com.crowcloud.gateway.suricataCore.events.EventPanelLoaded
import com.crowcloud.gateway.suricataCore.events.EventRecoverPassDone
import com.crowcloud.gateway.suricataCore.events.EventSignUpDone
import com.crowcloud.gateway2.R
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode

class LoginActivity : BaseActivity() {
    private fun navController() = findNavController(R.id.nav_host_fragment)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
    }

    override fun onSupportNavigateUp() = navController().navigateUp()

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onEvent(event: EventLoginDone){
        if(event.success){
            navController().navigate(R.id.choosePanelFragment)
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onEvent(event: EventSignUpDone){
        if(event.success){
            navController().navigateUp()
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onEvent(event: EventRecoverPassDone){
        if(event.success){
            navController().navigate(R.id.loginFragment)
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onEvent(event: EventPanelLoaded){
        startActivity(Intent(this, MainActivity::class.java))
        finish()
    }
}