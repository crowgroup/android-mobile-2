package com.crowcloud.gateway2.activity

import android.content.Intent
import android.os.Bundle
import android.support.v4.view.GravityCompat
import android.util.Log
import androidx.navigation.findNavController
import androidx.navigation.ui.NavigationUI
import androidx.navigation.ui.setupWithNavController
import com.crowcloud.gateway.suricataCore.activity.BaseActivity
import com.crowcloud.gateway.suricataCore.events.*
import com.crowcloud.gateway.suricataCore.network.model.response.User
import com.crowcloud.gateway2.R
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.app_bar_main.*
import kotlinx.android.synthetic.main.nav_header_main.*
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode

class MainActivity : BaseActivity() {

    companion object {
        const val TAG = "MainActivity"
    }

    private fun navController() = findNavController(R.id.nav_host_fragment)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)

        navController().addOnNavigatedListener { controller, dest ->
            run {
                Log.i(TAG, "navigate to: ${dest.label}")
            }
        }
        NavigationUI.setupWithNavController(toolbar, navController(), drawer_layout)
        NavigationUI.setupActionBarWithNavController(this, navController(), drawer_layout)

        nav_view.setupWithNavController(navController())
        post(EventRequestData(DataType.CurrentUser))
    }

    override fun onBackPressed() {
        if (drawer_layout.isDrawerOpen(GravityCompat.START)) {
            drawer_layout.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }

    override fun onSupportNavigateUp() = navController().navigateUp()

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onEvent(event: EventPanelLoaded) {
        navController().navigate(R.id.controlFragment)
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onEvent(event: EventRequestLogout) {
        startActivity(Intent(this, LoginActivity::class.java))
        finish()
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onEvent(event: EventReceivedData) {
        if (event.type == DataType.CurrentUser) {
            val user = event.data as User
            profileTitleTv.text = "${user.firstName} ${user.lastName}"
            profileDescTv.text = (event.data as User).email
            profileEditIb.setOnClickListener {
                navController().navigate(R.id.editProfileFragment)
                drawer_layout.closeDrawers()
            }
        }
    }
}