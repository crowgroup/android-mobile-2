package com.crowcloud.gateway2.login;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;

/**
 * Created by michael on 2/25/18.
 */

public class AuthenticatorService extends Service {
    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        AccountAutheticator autheticator = new AccountAutheticator(this);
        return autheticator.getIBinder();
    }
}
