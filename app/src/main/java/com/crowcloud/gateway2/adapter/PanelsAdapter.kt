package com.crowcloud.gateway2.adapter

import android.support.annotation.StringRes
import android.support.v4.content.ContextCompat
import android.widget.Button
import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.BaseViewHolder
import com.crowcloud.gateway2.App
import com.crowcloud.gateway2.R
import com.crowcloud.gateway.suricataCore.events.EventDeletePanel
import com.crowcloud.gateway.suricataCore.events.EventEditPanel
import com.crowcloud.gateway.suricataCore.events.EventSelectedPanel
import com.crowcloud.gateway.suricataCore.network.model.response.Panel

class PanelsAdapter : BaseQuickAdapter<Panel, BaseViewHolder>(R.layout.row_panel) {
    var selectedPos: Int = -1
    override fun convert(helper: BaseViewHolder?, item: Panel) {
        helper?.setVisible(R.id.ivIsSelected, helper.layoutPosition == selectedPos)
        helper?.setVisible(R.id.pbLoadPanel, helper.layoutPosition == selectedPos)
        helper?.setText(R.id.tvItem, item?.name)
        helper?.getView<Button>(R.id.btnDelete)?.setOnClickListener { App.get()?.eventBus?.post(EventDeletePanel(item)) }
        helper?.getView<Button>(R.id.btnEdit)?.setOnClickListener { App.get()?.eventBus?.post(EventEditPanel(item)) }

        when (item?.stateFull) {
            Panel.StateFull.Offline, null -> setItemOnlineMode(helper, false, R.string.offline)
            Panel.StateFull.Online -> setItemOnlineMode(helper, true, R.string.online)
            Panel.StateFull.Connected -> when (item?.pairType) {
                Panel.PairType.SAPI -> setItemOnlineMode(helper, false, R.string.connected_sapi)
                Panel.PairType.PC -> setItemOnlineMode(helper, false, R.string.connected_pc)
                Panel.PairType.CLOUD -> setItemOnlineMode(helper, true, R.string.connected_cloud)
                null -> setItemOnlineMode(helper, false, R.string.offline)
            }
        }

        helper?.itemView?.setOnClickListener {
            selectedPos = helper?.layoutPosition
            App.get()?.eventBus?.postSticky(EventSelectedPanel(item))
            notifyDataSetChanged()
        }
    }

    private fun setItemOnlineMode(helper: BaseViewHolder?, online: Boolean, @StringRes textResId: Int) {
        helper?.setImageResource(R.id.ivBall, if (online) R.drawable.notification_online else R.drawable.notification_offline)
        helper?.setTextColor(R.id.tvOnline, ContextCompat.getColor(mContext, if (online) R.color.green else R.color.redState))
        helper?.setText(R.id.tvOnline, textResId)
    }
}