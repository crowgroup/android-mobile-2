package com.crowcloud.gateway2.adapter

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentStatePagerAdapter
import com.crowcloud.gateway2.fragment.FragmentArea
import com.crowcloud.gateway.suricataCore.network.model.response.Area

class AreasPagerAdapter(fragmentManager: FragmentManager, data: List<Area>) : FragmentStatePagerAdapter(fragmentManager) {
    var areas: List<Area> = data
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    override fun getItem(position: Int): Fragment = FragmentArea.createInstance(position)
    override fun getCount(): Int = areas.size
    override fun getPageTitle(position: Int): CharSequence? = areas[position].name ?: ""
}