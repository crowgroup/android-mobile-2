package com.crowcloud.gateway2

import com.crowcloud.gateway.suricataCore.App
import com.crowcloud.gateway.suricataCore.login.LoginManager
import com.crowcloud.gateway.suricataCore.services.PushNotificationService
import com.crowcloud.gateway.suricataCore.util.isLollypop
import com.crowcloud.gateway2.activity.MainActivity

class App : com.crowcloud.gateway.suricataCore.App() {

    companion object {
        fun get(): App? = com.crowcloud.gateway.suricataCore.App.get()
    }

    override fun onCreate() {
        super.onCreate()
        PushNotificationService.initialize(if (isLollypop()) R.drawable.ic_crow_white else R.mipmap.ic_launcher,
                MainActivity::class, getString(R.string.push_notification_title))
        LoginManager.initialize(BuildConfig.APPLICATION_ID)
    }

}
