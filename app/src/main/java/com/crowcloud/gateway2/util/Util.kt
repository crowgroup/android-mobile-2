package com.crowcloud.gateway2.util

import com.crowcloud.gateway2.R

fun rssiIconResId(rssi: Int): Int = when (rssi) {
    in 11..15 -> R.drawable.signal_quality1
    in 16..19 -> R.drawable.signal_quality2
    in 20..23 -> R.drawable.signal_quality3
    in 24..27 -> R.drawable.signal_quality4
    in 28..Int.MAX_VALUE -> R.drawable.signal_quality5
    else -> R.drawable.signal_quality0
}