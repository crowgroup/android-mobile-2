package com.crowcloud.gateway2.fragment

import androidx.navigation.Navigation
import com.crowcloud.gateway2.App
import com.crowcloud.gateway2.R
import com.crowcloud.gateway.suricataCore.events.DataType
import com.crowcloud.gateway.suricataCore.events.EventRecoverPassword
import com.crowcloud.gateway.suricataCore.fragment.BaseFragment
import com.dd.processbutton.iml.ActionProcessButton
import kotlinx.android.synthetic.main.fragment_forgot_password.*

class ForgotPasswordFragment : BaseFragment() {

    override fun layoutResId(): Int = R.layout.fragment_forgot_password
    override fun dataType(): DataType? = null
    override fun showContentOnly(): Boolean  = true
    override fun requestData() {}

    override fun initView() {
        tvReturnLogin.setOnClickListener { Navigation.findNavController(it).navigateUp() }
        btnForgotPassSubmit.setOnClickListener{
            btnForgotPassSubmit.setMode(ActionProcessButton.Mode.ENDLESS)
            btnForgotPassSubmit.progress = 1
            App.get()?.eventBus?.post(EventRecoverPassword(etUserAccount.text.toString()))
        }
    }

    override fun onPause() {
        btnForgotPassSubmit.progress = 0
        super.onPause()
    }
}
