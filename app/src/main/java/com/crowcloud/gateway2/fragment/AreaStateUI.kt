package com.crowcloud.gateway2.fragment

import com.crowcloud.gateway2.R

enum class AreaStateUI {
    Disarmed(state_tvColorResId = R.color.greenState,state_tvStringResId = R.string.disarmed_title),
    Armed(R.drawable.btn_white, R.drawable.ic_disarm_gray, R.color.areaIconsColor, true,
            R.drawable.btn_red, R.drawable.ic_arm_white, R.color.white, false,
            R.drawable.btn_white, R.drawable.ic_stay_gray, R.color.areaIconsColor, false,
            R.drawable.btn_yellow, false, true,
            R.color.redState, R.string.arm_title),
    StayArmed(R.drawable.btn_white, R.drawable.ic_disarm_gray, R.color.areaIconsColor, true,
            R.drawable.btn_white, R.drawable.ic_arm_gray, R.color.areaIconsColor, false,
            R.drawable.btn_orange, R.drawable.ic_stay_white, R.color.white, false,
            R.drawable.btn_yellow, false, true,
            R.color.orangeState, R.string.stay_arm_title),
    ArmInProgress(state_tvColorResId = R.color.redState ,state_tvStringResId = R.string.arm_progress1),
    StayArmInProgress(state_tvColorResId = R.color.orangeState ,state_tvStringResId = R.string.stay_armed_progress1);

    val disarm_BackgroundResId: Int
    val disarm_ibResId: Int
    val disarm_tvColorResId: Int
    val disarm_Enabled: Boolean

    val arm_BackgroundResId: Int
    val arm_ibResId: Int
    val arm_tvColorResId: Int
    val arm_Enabled: Boolean

    val stay_BackgroundResId: Int
    val stay_ibResId: Int
    val stay_tvColorResId: Int
    val stay_Enabled: Boolean

    val panic_BackgroundResId: Int
    val panic_isAnimationOn: Boolean
    val panic_Enabled: Boolean

    val state_tvColorResId: Int
    val state_tvStringResId: Int

    constructor(disarmBackgroundResId: Int = R.drawable.btn_green, disarm_ibResId: Int = R.drawable.ic_disarm_white, disarm_tvColorResId: Int = R.color.white, disarm_Enabled: Boolean = true,
                arm_BackgroundResId: Int = R.drawable.btn_white, arm_ibResId: Int = R.drawable.ic_arm_gray, arm_tvColorResId: Int = R.color.areaIconsColor, arm_Enabled: Boolean = true,
                stay_BackgroundResId: Int = R.drawable.btn_white, stay_ibResId: Int = R.drawable.ic_stay_gray, stay_tvColorResId: Int = R.color.areaIconsColor, stay_Enabled: Boolean = true,
                panic_BackgroundResId: Int = R.drawable.btn_yellow, panic_isAnimationOn: Boolean = false, panic_Enabled: Boolean = true,
                state_tvColorResId: Int, state_tvStringResId: Int) {
        this.disarm_BackgroundResId = disarmBackgroundResId
        this.disarm_Enabled = disarm_Enabled
        this.disarm_ibResId = disarm_ibResId
        this.disarm_tvColorResId = disarm_tvColorResId
        this.arm_BackgroundResId = arm_BackgroundResId
        this.arm_ibResId = arm_ibResId
        this.arm_tvColorResId = arm_tvColorResId
        this.arm_Enabled = arm_Enabled
        this.stay_BackgroundResId = stay_BackgroundResId
        this.stay_ibResId = stay_ibResId
        this.stay_tvColorResId = stay_tvColorResId
        this.stay_Enabled = stay_Enabled
        this.panic_BackgroundResId = panic_BackgroundResId
        this.panic_isAnimationOn = panic_isAnimationOn
        this.panic_Enabled = panic_Enabled
        this.state_tvColorResId = state_tvColorResId
        this.state_tvStringResId = state_tvStringResId
    }
}