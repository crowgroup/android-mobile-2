package com.crowcloud.gateway2.fragment

import android.content.Context
import android.widget.TextView
import com.crowcloud.gateway2.R
import com.github.mikephil.charting.components.MarkerView
import com.github.mikephil.charting.data.Entry
import com.github.mikephil.charting.highlight.Highlight
import com.github.mikephil.charting.utils.MPPointF
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.TimeUnit

class MeasureStatMarkerView(context: Context, private val unit: String) : MarkerView(context, R.layout.custom_marker_view) {

    private val dateFormat = SimpleDateFormat("dd MMM HH:mm", Locale.getDefault())
    private val tvContent: TextView
    private val tvDateTime: TextView

    init {
        tvContent = findViewById(R.id.tvContent)
        tvDateTime = findViewById(R.id.tvDateTime)
    }

    override fun refreshContent(e: Entry?, highlight: Highlight?) {
        tvContent.text = String.format(Locale.getDefault(), "%.2f", e!!.y) + unit
        tvDateTime.text = dateFormat.format(Date(TimeUnit.MINUTES.toMillis(e.x.toLong())))
        super.refreshContent(e, highlight)
    }

    override fun getOffset() = MPPointF((-(width / 2)).toFloat(), (-height).toFloat())
}