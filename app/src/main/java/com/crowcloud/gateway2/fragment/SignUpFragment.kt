package com.crowcloud.gateway2.fragment

import com.crowcloud.gateway.suricataCore.events.DataType
import com.crowcloud.gateway.suricataCore.events.EventTrySignUp
import com.crowcloud.gateway.suricataCore.fragment.BaseFragment
import com.crowcloud.gateway2.R
import com.dd.processbutton.iml.ActionProcessButton
import kotlinx.android.synthetic.main.fragment_sign_up.*

class SignUpFragment : BaseFragment() {
    override fun requestData() {}
    override fun dataType(): DataType? = null
    override fun showContentOnly(): Boolean  = true
    override fun layoutResId(): Int = R.layout.fragment_sign_up

    override fun initView() {
        btnSignUpSubmit.setOnClickListener{
            btnSignUpSubmit.setMode(ActionProcessButton.Mode.ENDLESS)
            btnSignUpSubmit.progress = 1
            post(EventTrySignUp(etFNameReg.text.toString(),
                    etLNameReg.text.toString(),
                    etUserReg.text.toString(),
                    etPassReg.text.toString(),
                    etRetypePassReg.text.toString()))
        }

        tvReturnToLogin.setOnClickListener{navController().navigateUp()}
    }
}
