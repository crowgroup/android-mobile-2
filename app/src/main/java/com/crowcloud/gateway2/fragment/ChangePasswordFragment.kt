package com.crowcloud.gateway2.fragment

import android.graphics.Color
import android.support.design.widget.Snackbar
import android.support.v7.app.AlertDialog
import android.text.TextUtils
import com.crowcloud.gateway.suricataCore.events.DataType
import com.crowcloud.gateway.suricataCore.events.DataType.CurrentUser
import com.crowcloud.gateway.suricataCore.events.EventChangePassword
import com.crowcloud.gateway.suricataCore.events.EventChangePasswordDone
import com.crowcloud.gateway.suricataCore.fragment.BaseFragment
import com.crowcloud.gateway.suricataCore.util.NetworkUtil
import com.crowcloud.gateway.suricataCore.util.simple_callback.SimpleTextWatcher
import com.crowcloud.gateway2.R
import kotlinx.android.synthetic.main.change_password_fragment_layout.*
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode

const val PASSWORD_LENGTH = 6

class ChangePasswordFragment : BaseFragment() {
    override fun dataType(): DataType? = CurrentUser

    override fun requestData() = postRequestData(CurrentUser)

    override fun layoutResId(): Int = R.layout.change_password_fragment_layout

    override fun initView() {

        currentPassword.addTextChangedListener(object : SimpleTextWatcher() {
            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                currentPasswordTIL.isErrorEnabled = false
                currentPasswordTIL.error = null
            }
        })

        firstPassword.addTextChangedListener(object : SimpleTextWatcher() {
            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                firstPasswordTIL.isErrorEnabled = false
                firstPasswordTIL.error = null
            }
        })

        secondPassword.addTextChangedListener(object : SimpleTextWatcher() {
            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                secondPassworddTIL.isErrorEnabled = false
                secondPassworddTIL.error = null
            }
        })

        saveButton.setOnClickListener {
            val currentPassword = currentPassword.text.toString()
            val newPassword = firstPassword.text.toString()
            changePassword(currentPassword, newPassword)
        }
    }

    private fun changePassword(currentPassword: String, newPassword: String) {

        val isValidForChange = validateInput()
        if (!isValidForChange) {
            return
        }

        post(EventChangePassword(currentPassword, newPassword))
    }

    private fun validateInput(): Boolean {
        var result = true
        val _currentPassword = currentPassword.text.toString()
        val _firstPassword = firstPassword.text.toString()
        val _secondPassword = secondPassword.text.toString()

        if (TextUtils.isEmpty(_currentPassword)) {
            currentPasswordTIL.isErrorEnabled = true
            currentPasswordTIL.error = getString(R.string.change_password_field_empty_error)
            result = false
        }

        if (TextUtils.isEmpty(_firstPassword)) {
            firstPasswordTIL.isErrorEnabled = true
            firstPasswordTIL.error = getString(R.string.change_password_field_empty_error)
            result = false
        } else if (firstPassword.length() < PASSWORD_LENGTH) {
            firstPasswordTIL.isErrorEnabled = true
            firstPasswordTIL.error = getString(R.string.change_password_password_bad)
            result = false
        }

        if (TextUtils.isEmpty(_secondPassword)) {
            secondPassworddTIL.isErrorEnabled = true
            secondPassworddTIL.error = getString(R.string.change_password_field_empty_error)
            result = false
        } else if (secondPassword.length() < PASSWORD_LENGTH) {
            secondPassworddTIL.isErrorEnabled = true
            secondPassworddTIL.error = getString(R.string.change_password_password_bad)
            result = false
        } else if (TextUtils.isEmpty(_firstPassword) || _firstPassword != _secondPassword) {
            secondPassworddTIL.isErrorEnabled = true
            secondPassworddTIL.error = getString(R.string.change_password_password_mismatch)
            result = false
        }

        if (!NetworkUtil.isNetworkAvailable) {
            AlertDialog.Builder(activity!!)
                    .setTitle(R.string.error)
                    .setMessage(R.string.no_network_connection)
                    .setPositiveButton(R.string.ok, null)
                    .show()
            return false
        }

        return result
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onDone(event: EventChangePasswordDone) {
        if (event.success) {
            Snackbar.make(view!!, "Change passsword succeeded!", Snackbar.LENGTH_LONG).setAction("OK") {
                view?.let { navController().navigateUp() }
            }.setActionTextColor(Color.GREEN).show()
        }
    }

}