package com.crowcloud.gateway2.fragment

import com.crowcloud.gateway2.R
import com.crowcloud.gateway.suricataCore.events.DataType
import com.crowcloud.gateway.suricataCore.events.DataType.CurrentPanel
import com.crowcloud.gateway.suricataCore.events.DataType.CurrentUser
import com.crowcloud.gateway.suricataCore.events.EventReceivedData
import com.crowcloud.gateway.suricataCore.events.EventUpdateNotificationFilters
import com.crowcloud.gateway.suricataCore.fragment.BaseFragment
import com.crowcloud.gateway.suricataCore.network.model.response.NotificationsFilter
import com.crowcloud.gateway.suricataCore.network.model.response.Panel
import com.crowcloud.gateway.suricataCore.network.model.response.User
import kotlinx.android.synthetic.main.fragment_notifications.*
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode

class NotificationsFragment : BaseFragment() {
    override fun dataType(): DataType? = CurrentPanel

    override fun requestData() = postRequestData(CurrentPanel, CurrentUser)

    override fun layoutResId(): Int = R.layout.fragment_notifications

    override fun initView() {
        tvEmailGetNotified.setOnClickListener { /*navController().navigate(R.id.notificationsEmails)*/ }
        updateBtn.setOnClickListener {
            post(EventUpdateNotificationFilters(Panel().apply {
                receivePictures = cbRcvPic.isChecked
                notificationsFilter = NotificationsFilter().apply {
                    alarm = cbAlarm.isChecked
                    arm = cbArm.isChecked
                    configuration = cbConfiguration.isChecked
                    information = cbInformation.isChecked
                    takePicture = cbTakePic.isChecked
                    troubles = cbTroubles.isChecked
                    userAssociation = cbUserAssociation.isChecked

                }
            }))
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN_ORDERED)
    fun onData(event: EventReceivedData) {
        if (event.type == CurrentPanel && event.data is Panel) {
            (event.data as Panel).notificationsFilter?.apply {
                cbAlarm.isChecked = alarm
                cbArm.isChecked = arm
                cbConfiguration.isChecked = configuration
                cbInformation.isChecked = information
                cbTakePic.isChecked = takePicture
                cbTroubles.isChecked = troubles
                cbUserAssociation.isChecked = userAssociation
                cbRcvPic.isChecked = (event.data as Panel).receivePictures
            }
        } else if(event.type == CurrentUser && event.data is User){
            defaultNotifEmailTv.text = getString(R.string.notificationDefaultEmailFormat, (event.data as User).email)
        }
    }
}