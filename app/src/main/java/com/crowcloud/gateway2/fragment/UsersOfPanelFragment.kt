package com.crowcloud.gateway2.fragment

import android.os.Bundle
import android.view.View
import com.chad.library.adapter.base.BaseViewHolder
import com.crowcloud.gateway.suricataCore.events.DataType
import com.crowcloud.gateway.suricataCore.events.EventDeletePanelUser
import com.crowcloud.gateway.suricataCore.fragment.BaseListFragment
import com.crowcloud.gateway.suricataCore.network.model.response.PanelUser
import com.crowcloud.gateway2.R
import com.crowcloud.gateway2.fragment.PanelUserFragment.Companion.PANEL_USER_KEY
import kotlinx.android.synthetic.main.fragment_users_of_panel.*

class UsersOfPanelFragment : BaseListFragment<PanelUser>() {
    override fun itemLayoutResId() = R.layout.row_user_of_panel
    override fun dataType(): DataType = DataType.UsersOfPanel
    override fun layoutResId() = R.layout.fragment_users_of_panel

    override fun initView() {
        super.initView()
        fbAdd.setOnClickListener { navController().navigate(R.id.panelUserFragment) }
    }

    override fun convertItemToView(helper: BaseViewHolder, item: PanelUser) {
        helper.setText(R.id.tvUserName, item.name ?: getString(R.string.no_user_name))
        helper.getView<View>(R.id.rlUserRowMain).setOnClickListener {
            navController().navigate(R.id.panelUserFragment, Bundle().apply { putSerializable(PANEL_USER_KEY, item) })
        }
        helper.getView<View>(R.id.btnDelete).setOnClickListener {
            post(EventDeletePanelUser(item))
        }
        helper.getView<View>(R.id.btnEdit).setOnClickListener {
            navController().navigate(R.id.panelUserFragment, Bundle().apply { putSerializable(PANEL_USER_KEY, item) })
        }
    }
}