package com.crowcloud.gateway2.fragment

import com.crowcloud.gateway.suricataCore.events.*
import com.crowcloud.gateway.suricataCore.events.DataType.MaintenanceState
import com.crowcloud.gateway.suricataCore.events.DataType.SirenEnabledState
import com.crowcloud.gateway.suricataCore.fragment.BaseFragment
import com.crowcloud.gateway2.R
import kotlinx.android.synthetic.main.fragment_system_settings.*
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode
import java.text.SimpleDateFormat
import java.util.*

val dateFormat = SimpleDateFormat("MMM dd, yyyy hh:mm:ss", Locale.getDefault())

class SysSettingsFragment : BaseFragment() {
    override fun dataType(): DataType? = null

    override fun requestData() = postRequestData(MaintenanceState, SirenEnabledState)

    override fun layoutResId(): Int = R.layout.fragment_system_settings

    override fun initView() {
        tvRstPnlLoc.setOnClickListener { post(EventUpdatePanelConnection()) }
        tvUpdatePnlLoc.setOnClickListener { post(EventUpdatePanelLocation()) }
        tvPanelUsers.setOnClickListener { navController().navigate(R.id.userAccountsFragment) }
        tvNotifications.setOnClickListener { navController().navigate(R.id.notificationsFragment) }
        tvUsersOfPanel.setOnClickListener { navController().navigate(R.id.usersOfPanelFragment) }
        tvLogout.setOnClickListener { post(EventRequestLogout()) }
        tvAccessManage.setOnCheckedChangeListener { v, isChecked -> post(EventUpdateMaintenanceState(isChecked)) }
        tbDisableSiren.setOnCheckedChangeListener { v, isChecked -> post(EventUpdateSirenEnabledState(isChecked)) }
    }

    @Subscribe(threadMode = ThreadMode.MAIN_ORDERED)
    fun onData(event: EventReceivedData) {
        if (event.type == MaintenanceState) {
            val state = event.data as com.crowcloud.gateway.suricataCore.network.model.response.MaintenanceState
            val isOpen = state.maintenanceUntil != null && state.maintenanceUntil!!.after(Date())
            tvAccessManage.isChecked = isOpen
            tvAccessManage.text = if (!isOpen) getString(R.string.no_access) else "Available until: \n${dateFormat.format(state.maintenanceUntil)}"
        } else if (event.type == SirenEnabledState) {
            val state = event.data as Boolean
            tbDisableSiren.isChecked = state
        }
    }
}