package com.crowcloud.gateway2.fragment

import android.support.v7.view.ContextThemeWrapper
import android.support.v7.widget.PopupMenu
import android.view.MenuInflater
import android.view.View
import com.crowcloud.gateway2.R
import com.crowcloud.gateway2.adapter.PanelsAdapter
import com.crowcloud.gateway.suricataCore.events.DataType
import com.crowcloud.gateway.suricataCore.fragment.BaseListFragment
import com.crowcloud.gateway.suricataCore.network.model.response.Panel
import kotlinx.android.synthetic.main.fragment_choose_panel.*

class ChoosePanelFragment : BaseListFragment<Panel>() {
    override fun dataType(): DataType = DataType.Panels
    override fun createAdapter() = PanelsAdapter()
    override fun layoutResId(): Int = R.layout.fragment_choose_panel

    override fun initView() {
        super.initView()
        fbAddPanel.setOnClickListener { openPanelConfigPopupMenu(it) }
    }

    fun openPanelConfigPopupMenu(view: View) {
        val wrapper = ContextThemeWrapper(activity, R.style.PopupMenuStyle)
        val popupMenu = PopupMenu(wrapper, view)
        val inflater = MenuInflater(activity)
        inflater.inflate(R.menu.popup_config_add_panel, popupMenu.menu)

        popupMenu.setOnMenuItemClickListener { item ->
            when (item.itemId) {
                R.id.action_add_panel -> {
                    true
                }
                R.id.action_config_wifi -> {
                    true
                }
                else -> false
            }
        }
        popupMenu.show()
    }
}