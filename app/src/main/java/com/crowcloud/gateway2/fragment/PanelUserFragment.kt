package com.crowcloud.gateway2.fragment

import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.text.TextUtils
import android.widget.EditText
import android.widget.TextView
import com.crowcloud.gateway.suricataCore.events.DataType
import com.crowcloud.gateway.suricataCore.events.EventCreateOrUpdatePanelUser
import com.crowcloud.gateway.suricataCore.events.EventDeletePanelUser
import com.crowcloud.gateway.suricataCore.fragment.BaseFragment
import com.crowcloud.gateway.suricataCore.network.model.response.PanelUser
import com.crowcloud.gateway.suricataCore.util.safeParse
import com.crowcloud.gateway2.R
import kotlinx.android.synthetic.main.fragment_panel_user.*

const val MAX_PENDANT = 16777214

class PanelUserFragment : BaseFragment() {
    companion object {
        const val PANEL_USER_KEY = "panelUser"

    }

    override fun requestData() {}

    override fun dataType(): DataType? = null

    override fun layoutResId(): Int = R.layout.fragment_panel_user

    override fun initView() {
        val user = arguments?.getSerializable(PANEL_USER_KEY) as PanelUser?
        ibDialogClose.setOnClickListener { navController().navigateUp() }
        ibDialogDelete.setOnClickListener {
            post(EventDeletePanelUser(user))
            navController().navigateUp()
        }
        btnNegative.setOnClickListener { navController().navigateUp() }
        multipleToggleSwitchAreas.dividerDrawable = ColorDrawable(Color.RED)
        multipleToggleSwitchAreas.setLabels(arrayListOf("Area1", "Area2", "Area3", "Area4"))
        tvUpdateUserInfoTitle.text = getString(R.string.add_user)

        user?.apply {
            etUserName.setText(name)
            etUserCode.setText(userCode)
            etUserCode.setOnFocusChangeListener { v, hasFocus ->
                if (!hasFocus && !TextUtils.isEmpty(etUserCode.text)) {
                    btnPositive.isEnabled = validateUserCode(etUserCode)
                }
            }
            cbRemote.isChecked = remote == true
            etPendant.setText("$pendantId")
            etPendant.setOnFocusChangeListener { v, hasFocus ->
                if (!hasFocus && !TextUtils.isEmpty(etPendant.text)) {
                    btnPositive.isEnabled = validatePendant(etPendant)
                }
            }
            cbDisarm.isChecked = disarm == true
            cbStay.isChecked = stay == true
            cbArm.isChecked = arm == true
            cbDisarmStay.isChecked = disarmStay == true
            tvUpdateUserInfoTitle.text = getString(R.string.edit_user)

            areas?.forEach { multipleToggleSwitchAreas.setCheckedTogglePosition(it) }

            cbChangeHisCode.isChecked = canEditHimself == true
            cbChangeAllCode.isChecked = canEditAllUsers == true
        }


        // if button is clicked, close the custom dialog
        btnPositive.setOnClickListener {
            if (validIsEmpty(arrayListOf(etUserName, etUserCode)) && validateUserCode(etUserCode) && validatePendant(etPendant)) {
                val userNew = PanelUser().apply {
                    user?.let {
                        id = it.id
                        pendantId = it.pendantId
                        userCode = it.userCode
                    }

                    name = etUserName.text.toString()
                    remote = cbRemote.isChecked
                    stay = cbStay.isChecked
                    disarm = cbDisarm.isChecked
                    canEditHimself = cbChangeHisCode.isChecked

                    val pendId = safeParse(etPendant.text.toString(), -1)
                    if (pendId in 0..MAX_PENDANT) {
                        pendantId = pendId
                    }
                    if (etUserCode.text.isNotEmpty()) {
                        userCode = etUserCode.text.toString()
                    }
                    disarmStay = cbDisarmStay.isChecked
                    canEditAllUsers = cbChangeAllCode.isChecked
                    arm = cbArm.isChecked
                    areas = ArrayList<Int>(multipleToggleSwitchAreas.checkedTogglePositions)
                }
                post(EventCreateOrUpdatePanelUser(userNew))
                navController().navigateUp()
            }
        }
    }

    private fun validatePendant(etPendant: EditText): Boolean {
        if (TextUtils.isEmpty(etPendant.text)) {
            return true
        }
        val pendant = etPendant.text.toString()
        if (pendant.length < 9) {
            if (safeParse(pendant, -1) in 0..MAX_PENDANT) {
                return true
            }
        }
        etPendant.error = getString(R.string.pendant_vlidate_error)

        return false
    }

    private fun validateUserCode(etUserCode: EditText): Boolean {

        if (etUserCode.text.toString().isEmpty()) {
            return true
        }

        if (etUserCode.text.toString() == "0000") {
            etUserCode.error = getString(R.string.four_zero_error)
            return false
        }

        val userCode = etUserCode.text.toString()
        if (!TextUtils.isDigitsOnly(userCode) || userCode.length < 4 || userCode.length > 8) {
            etUserCode.error = getString(R.string.user_code_invalidate_error)
            return false
        }

        return true
    }


    private fun validIsEmpty(arrValid: ArrayList<TextView>): Boolean {
        var isValid = true
        arrValid.forEach {
            if (it.text.isEmpty()) {
                it.error = getString(R.string.emptyField_error)
                isValid = false
            }
        }
        return isValid
    }
}