package com.crowcloud.gateway2.fragment

import android.widget.Button
import android.widget.TextView
import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.BaseViewHolder
import com.crowcloud.gateway2.R
import com.crowcloud.gateway.suricataCore.events.DataType
import com.crowcloud.gateway.suricataCore.events.EventToggleOutputDone
import com.crowcloud.gateway.suricataCore.events.EventToggleOutputRequest
import com.crowcloud.gateway.suricataCore.fragment.BaseListFragment
import com.crowcloud.gateway.suricataCore.network.model.response.Output
import com.crowcloud.gateway.suricataCore.util.getColor
import com.crowcloud.gateway2.util.rssiIconResId
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode

class OutputsFragment : BaseListFragment<Output>() {
    override fun dataType(): DataType = DataType.Outputs

    private val inProgressIds: ArrayList<Int> = ArrayList()

    override fun createAdapter(): BaseQuickAdapter<Output, in BaseViewHolder>? =
            object : BaseQuickAdapter<Output, BaseViewHolder>(R.layout.row_output) {
                override fun convert(helper: BaseViewHolder?, item: Output?) {
                    helper?.setText(R.id.tvOutputName, item?.name)
                    helper?.setChecked(R.id.tbOutputActive, item?.state == true)

                    helper?.setText(R.id.tvRssiStatus, if(item?.rssi ?: 0 > 0)"${item?.rssi}%" else "")
                    helper?.getView<TextView>(R.id.tvRssiStatus)?.setCompoundDrawablesWithIntrinsicBounds(0,0,0, rssiIconResId(item?.rssi ?: 0))
                    val troubles : String? = item?.troublesList?.joinToString(separator = ",")
                    helper?.setTextColor(R.id.tvOutputStatus, getColor( if(troubles.isNullOrEmpty()) R.color.green else R.color.red))
                    helper?.setText(R.id.tvOutputStatus, if(troubles.isNullOrEmpty()) getString(R.string.ready_regular) else troubles )

                    helper?.setVisible(R.id.pbOutput, inProgressIds.contains(item?.id))

                    helper?.getView<Button>(R.id.tbOutputActive)?.apply {
                        setOnClickListener {
                            item?.let { post(EventToggleOutputRequest(it)) }
                            item?.id?.let { inProgressIds.add(it) }
                            notifyItemChanged(helper.layoutPosition)
                        }
                        isEnabled = !inProgressIds.contains(item?.id)
                    }
                }
            }

    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    fun onData(event: EventToggleOutputDone){
        inProgressIds.remove(event.output.id)
        if(event.error == null) {
            val idx = adapter?.data?.indexOfFirst { it.id == event.output.id } ?: 0
            adapter?.remove(idx)
            adapter?.addData(idx, event.output)
            adapter?.notifyItemChanged(idx)
        }
    }
}