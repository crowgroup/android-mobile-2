package com.crowcloud.gateway2.fragment

import android.graphics.Color
import android.support.design.widget.TabLayout
import com.crowcloud.gateway.suricataCore.components.ComponentData
import com.crowcloud.gateway.suricataCore.events.DataType
import com.crowcloud.gateway.suricataCore.events.EventReceivedData
import com.crowcloud.gateway.suricataCore.events.EventRequestData
import com.crowcloud.gateway.suricataCore.extensions.invisible
import com.crowcloud.gateway.suricataCore.fragment.BaseFragment
import com.crowcloud.gateway.suricataCore.network.model.response.Measurement
import com.crowcloud.gateway.suricataCore.network.services.MeasurementsApi
import com.crowcloud.gateway.suricataCore.util.getDrawable
import com.crowcloud.gateway.suricataCore.util.simple_callback.SimpleTabSelectedListener
import com.crowcloud.gateway2.R
import com.github.mikephil.charting.components.XAxis
import com.github.mikephil.charting.components.YAxis
import com.github.mikephil.charting.data.Entry
import com.github.mikephil.charting.data.LineData
import com.github.mikephil.charting.data.LineDataSet
import com.github.mikephil.charting.formatter.IAxisValueFormatter
import kotlinx.android.synthetic.main.fragment_measurement_graph.*
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.TimeUnit

const val ARG_MEASUREMENT = "measures"
const val ARG_TYPE = "Type"

class MeasureStatsFragment : BaseFragment() {
    override fun dataType(): DataType? = DataType.MeasurementStats
    override fun layoutResId(): Int = R.layout.fragment_measurement_graph

    private val hourFormat = SimpleDateFormat("HH:mm", Locale.getDefault())
    private val dayFormat = SimpleDateFormat("dd MMM", Locale.getDefault())

    private var measure: Measurement? = null
    private var mv: MeasureStatMarkerView? = null
    private var isInProcess = false
    private var offset: Int = 0
    private var duration: MeasurementsApi.TimeUnit = MeasurementsApi.TimeUnit.hour

    override fun requestData() {
        measureType = arguments?.getSerializable(ARG_TYPE) as Measurement.MeasureType
        measure = arguments?.getSerializable(ARG_MEASUREMENT) as Measurement?
        measure?.let {
            post(EventRequestData(DataType.MeasurementStats,
                    requestData = ComponentData.MeasurementStatsRequestData(it, 1, duration, offset)))
        }
        mv?.invalidate()
    }

    private lateinit var measureType: Measurement.MeasureType

    override fun initView() {
        btnNext?.setOnClickListener { v ->
            if (offset > 0 && !isInProcess) {
                isInProcess = true
                offset--
                requestData()
            }
        }
        btnPrevious?.setOnClickListener { v ->
            if (!isInProcess) {
                isInProcess = true
                offset++
                requestData()
            }
        }
        tabTypeGraphPeriod?.addOnTabSelectedListener(object : SimpleTabSelectedListener() {
            override fun onTabSelected(tab: TabLayout.Tab) {
                offset = 0
                duration = when (tab.position) {
                    1 -> MeasurementsApi.TimeUnit.day
                    2 -> MeasurementsApi.TimeUnit.month
                    3 -> MeasurementsApi.TimeUnit.year
                    else -> MeasurementsApi.TimeUnit.hour
                }
                requestData()
            }
        })
        mv = MeasureStatMarkerView(context!!, measure?.getUnit(measureType) ?: "")
    }

    private fun setData(data: List<Measurement>) {
        var entries: List<Entry> = data.map { m ->
            Entry(TimeUnit.MILLISECONDS.toMinutes(m.data?.panelTime?.time ?: 0L).toFloat(),
                    (m.getValue(measureType) ?: 0F) / 100)
        }.sortedBy { it.x }

        chart?.apply {
            description?.isEnabled = false
            setTouchEnabled(true)
            dragDecelerationFrictionCoef = 0.9f
            isDragEnabled = true
            setScaleEnabled(false)
            setDrawGridBackground(false)
            isHighlightPerDragEnabled = true
            setViewPortOffsets(0f, 0f, 0f, 16f)
            marker = mv
            legend?.isEnabled = false

            this.data = LineData(LineDataSet(entries, "data").apply {
                axisDependency = YAxis.AxisDependency.LEFT
                lineWidth = 1.5f
                fillAlpha = 65
                setDrawFilled(true)
                setDrawValues(false)
                setDrawCircles(true)
                setDrawCircleHole(false)
                isHighlightEnabled = true
                highLightColor = Color.BLACK
                mode = LineDataSet.Mode.HORIZONTAL_BEZIER
                fillDrawable = getDrawable(R.drawable.shape_fill_graph)
                color = Color.WHITE
                valueTextColor = Color.WHITE
            })

            xAxis?.apply {
                position = XAxis.XAxisPosition.BOTTOM_INSIDE
                setDrawLabels(true)
                textSize = 13f
                textColor = Color.WHITE
                setDrawAxisLine(true)
                setDrawGridLines(true)
                xOffset = 10f
                yOffset = 10f

                val min = entries.first().x
                val max = entries.last().x
                val delta = (max - min) / 9

                axisMinimum = min - delta
                axisMaximum = max + delta
                setCenterAxisLabels(false)
                granularity = 1f // one hour

                valueFormatter = IAxisValueFormatter { value, axis ->
                    val tmp = (if (duration == MeasurementsApi.TimeUnit.hour) hourFormat else dayFormat).format(Date(TimeUnit.MINUTES.toMillis(value.toLong())))
                    tmp
                }
            }

            axisLeft?.apply {
                setPosition(YAxis.YAxisLabelPosition.INSIDE_CHART)
                setDrawGridLines(true)
                isGranularityEnabled = true
                axisMinimum = getMinValues(data) - 1f
                axisMaximum = getMaxValues(data) + 1f
                yOffset = -9f
                textColor = Color.WHITE
                textSize = 13f
            }
        }

        chart?.invalidate()
    }

    private fun loadChart(data: List<Measurement>) {
        btnNext?.invisible(offset == 0)
        btnPrevious?.invisible(offset == 1)
        mv?.chartView = chart
        setData(data)
    }

    private fun getMaxValues(data: List<Measurement>): Float =
            (Collections.max(data) { o1, o2 ->
                ((o1.getValue(measureType)?.toInt() ?: 0) - (o2.getValue(measureType)?.toInt()
                        ?: 0))
            }.getValue(measureType) ?: 0F) / 100

    private fun getMinValues(data: List<Measurement>): Float =
            (Collections.min(data) { o1, o2 ->
                ((o1.getValue(measureType)?.toInt() ?: 0) - (o2.getValue(measureType)?.toInt()
                        ?: 0))
            }.getValue(measureType) ?: 0F) / 100

    @Subscribe(threadMode = ThreadMode.MAIN_ORDERED)
    fun onData(event: EventReceivedData) {
        if (event.type == DataType.MeasurementStats) {
            isInProcess = false
            if (event.data is List<*>) {
                val data = event.data as List<Measurement>
                loadChart(data)
            }
        }
    }
}