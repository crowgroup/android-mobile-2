package com.crowcloud.gateway2.fragment

import android.os.Bundle
import android.os.Handler
import android.support.v7.app.AlertDialog
import android.util.Log
import android.view.animation.AnimationUtils
import android.widget.TextView
import com.crowcloud.gateway.suricataCore.events.*
import com.crowcloud.gateway.suricataCore.extensions.visible
import com.crowcloud.gateway.suricataCore.fragment.BaseFragment
import com.crowcloud.gateway.suricataCore.network.model.request.AreaStateRequest
import com.crowcloud.gateway.suricataCore.network.model.response.Area
import com.crowcloud.gateway.suricataCore.network.model.response.Panel
import com.crowcloud.gateway.suricataCore.network.model.response.Troubles
import com.crowcloud.gateway.suricataCore.network.model.response.Zone
import com.crowcloud.gateway.suricataCore.persistence.Prefs
import com.crowcloud.gateway.suricataCore.util.*
import com.crowcloud.gateway.suricataCore.util.ui.UserCodeDialog
import com.crowcloud.gateway2.R
import kotlinx.android.synthetic.main.fragment_area.*
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode

class FragmentArea : BaseFragment() {

    companion object {
        const val TAG: String = "FragmentArea"
        const val KEY_ARGUMENT_AREA_ID: String = "AreaId"

        fun createInstance(areaId: Int): FragmentArea = FragmentArea().apply {
            arguments = Bundle().apply { putInt(KEY_ARGUMENT_AREA_ID, areaId) }
        }
    }

    override fun dataType(): DataType? = DataType.Area
    override fun showContentOnly(): Boolean = true
    override fun layoutResId(): Int = R.layout.fragment_area

    private var area: Area? = null
    private var panel: Panel? = null
    private var troubles: Troubles? = null
    private var zones: List<Zone>? = null

    override fun requestData() {
        post(EventRequestData(DataType.Zones, requestData = areaId()))
        post(EventRequestData(DataType.Troubles))
        post(EventRequestData(DataType.Area, requestData = areaId()))
        post(EventRequestData(DataType.CurrentPanel))
    }

    private fun areaId() = arguments?.getInt(KEY_ARGUMENT_AREA_ID)

    override fun initView() {
        frDisarm.setOnClickListener {
            area?.run {
                if (isDeviceScreenLocked()) {
                    post(EventRequestUpdateAreaState(id, AreaStateRequest.State.DISARM, true))
                } else {
                    UserCodeDialog(context!!, validate = { it == panel?.userCode }) {
                        post(EventRequestUpdateAreaState(id, AreaStateRequest.State.DISARM, true))
                    }.show()
                }
            }
        }
        frArm.setOnClickListener { area?.run { post(EventRequestUpdateAreaState(id, AreaStateRequest.State.ARM)) } }
        frStayArm.setOnClickListener { area?.run { post(EventRequestUpdateAreaState(id, AreaStateRequest.State.STAY)) } }
        frPanic.setOnClickListener { area?.run { post(EventRequestPanicAlarm(id)) } }
    }

    @Subscribe(threadMode = ThreadMode.MAIN_ORDERED)
    fun onNotReady(event: EventRequestUpdateAreaStateNotReady) {
        if (event.event.areaId == areaId()) {
            AlertDialog.Builder(context!!).setTitle("Area is not ready to ${getAreaStateName(event.event.newState)}")
                    .setMessage("Please fix the troubles in panel and zones and try again, or Force ${getAreaStateName(event.event.newState)}")
                    .setPositiveButton("Force ${getAreaStateName(event.event.newState)}") { d, w -> post(event.event.apply { force = true }) }
                    .setNegativeButton(R.string.cancel) { d, w -> }
                    .setNeutralButton(R.string.show_troubles) { d, w -> navController().navigate(R.id.troublesFragment) }
                    .show()
        }
    }

    fun getAreaStateName(state: AreaStateRequest.State) = when (state) {
        AreaStateRequest.State.ARM -> "Arm"
        AreaStateRequest.State.DISARM -> "Disarm"
        AreaStateRequest.State.STAY -> "Stay arm"
    }

    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN_ORDERED)
    fun onData(event: EventReceivedData) {
        if (event.data is Throwable) {//TODO: handle errors
            Log.e(TAG, "onData received error", event.data as Throwable)
            return
        }
        Log.i(TAG, "received data - ${event.type} : ${event.data}")

        when (event.type) {
            DataType.Areas -> area = (event.data as List<Area>).find { it.id == areaId() }
            DataType.Area -> if (event.data is Area) {
                if ((event.data as Area).id == areaId()) {
                    area = event.data as Area
                }
            }
            DataType.Troubles -> troubles = event.data as Troubles
            DataType.CurrentPanel -> panel = event.data as Panel
            DataType.Zones -> zones = event.data as List<Zone>
            else -> {
                return
            }
        }

        setupUI()
    }

    private fun setupUI() {
        Log.i(TAG, "setupUI()")

        setupUiPanic(troubles?.panicAlarm == true)

        tvAreaState.visible(true)
        when (area?.state) {
            Area.AreaState.Disarmed, null -> setupUiAreaState(AreaStateUI.Disarmed)
            Area.AreaState.Armed -> setupUiAreaState(AreaStateUI.Armed)
            Area.AreaState.StayArmed -> setupUiAreaState(AreaStateUI.StayArmed)
            Area.AreaState.ArmInProgress -> setupUiAreaState(AreaStateUI.ArmInProgress)
            Area.AreaState.StayArmInProgress -> setupUiAreaState(AreaStateUI.StayArmInProgress)
        }

        ivArmAlert.visible(area?.isReadyToArm == false && area?.state == Area.AreaState.Disarmed)
        ivStayAlert.visible(area?.isReadyToStay == false && area?.state == Area.AreaState.Disarmed)

        if (area?.isZone24HAlarm == true || area?.isZoneAlarm == true) {
            tvAreaState.setTextColor(getColor(R.color.red))
            tvAreaState.text = getString(R.string.alarm_state)
        } else {
            tvAreaState.setTextColor(getColor(R.color.greenState))
        }

        setCountDown(getArmDelayTSPref(areaId()), Area.AreaState.ArmInProgress, tvArmCountDown)
        setCountDown(getStayDelayTSPref(areaId()), Area.AreaState.StayArmInProgress, tvStayCountDown)

        if (zones?.isEmpty() == true) {
            arrayOf(frDisarm, frPanic, frArm, frStayArm).forEach { it.isEnabled = false }
            tvAreaState.text = getString(R.string.no_zone)
        }
    }

    private fun setCountDown(delayTsPref: Prefs.Long?, progressState: Area.AreaState, tvCountDown: TextView) {
        if (isAdded) {
            val delay = ((delayTsPref?.get() ?: 0L) - now()) / 1000
            if (delay > 0 && area?.state == progressState) {
                tvCountDown.text = delay.toString()
                tvCountDown.visible(true)
                Handler().postDelayed({
                    setCountDown(delayTsPref, progressState, tvCountDown)
                    if (delay <= 1) Handler().postDelayed({ post(EventRequestData(DataType.Area, requestData = areaId(), useCache = false)) }, 2000)
                }, 1000)
            } else {
                tvCountDown.visible(false)
            }
        }
    }

    private fun setupUiPanic(isOn: Boolean) {
        frPanic.background = getDrawable(if (isOn) R.drawable.btn_red else R.drawable.btn_yellow)
        frPanic.isEnabled = !isOn
        if (isOn) {
            frPanic.startAnimation(AnimationUtils.loadAnimation(activity, R.anim.blink))
        } else {
            frPanic.clearAnimation()
        }
    }

    private fun setupUiAreaState(uiState: AreaStateUI) {
        frDisarm.background = getDrawable(uiState.disarm_BackgroundResId)
        frDisarm.isEnabled = uiState.disarm_Enabled
        ibDisarm.setImageDrawable(getDrawable(uiState.disarm_ibResId))
        tvDisarmStatus.setTextColor(getColor(uiState.disarm_tvColorResId))

        tvAreaState.setTextColor(getColor(uiState.state_tvColorResId))
        tvAreaState.text = getString(uiState.state_tvStringResId)

        frArm.background = getDrawable(uiState.arm_BackgroundResId)
        frArm.isEnabled = uiState.arm_Enabled
        ivArm.setImageDrawable(getDrawable(uiState.arm_ibResId))
        tvArmStatus.setTextColor(getColor(uiState.arm_tvColorResId))

        frStayArm.background = getDrawable(uiState.stay_BackgroundResId)
        frStayArm.isEnabled = uiState.stay_Enabled
        ibStayarm.setImageDrawable(getDrawable(uiState.stay_ibResId))
        tvStayStatus.setTextColor(getColor(uiState.stay_tvColorResId))
    }
}
