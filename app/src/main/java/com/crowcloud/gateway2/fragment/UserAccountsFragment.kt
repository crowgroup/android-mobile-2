package com.crowcloud.gateway2.fragment

import android.widget.TextView
import com.chad.library.adapter.base.BaseViewHolder
import com.crowcloud.gateway.suricataCore.events.DataType
import com.crowcloud.gateway.suricataCore.events.EventDeleteUserAccount
import com.crowcloud.gateway.suricataCore.events.EventReceivedData
import com.crowcloud.gateway.suricataCore.fragment.BaseListFragment
import com.crowcloud.gateway.suricataCore.network.model.response.User
import com.crowcloud.gateway.suricataCore.network.model.response.UserAccount
import com.crowcloud.gateway.suricataCore.util.getColor
import com.crowcloud.gateway2.R
import com.daimajia.swipe.SwipeLayout
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode

class UserAccountsFragment : BaseListFragment<UserAccount>() {
    override fun itemLayoutResId() = R.layout.row_panel_user
    override fun dataType(): DataType = DataType.Users

    override fun requestData() {
        super.requestData()
        postRequestData(DataType.CurrentUser)
    }

    override fun convertItemToView(helper: BaseViewHolder, item: UserAccount) {
        helper.setText(R.id.tvPName, item.user?.firstName)
        helper.setText(R.id.tvFName, item.user?.lastName)
        helper.setText(R.id.tvEmail, item.user?.email)
        helper.getView<TextView>(R.id.btnDelete).setOnClickListener {
            if (currentUser?.isSuperuser == true && item.user?.pk != currentUser?.pk) {
                post(EventDeleteUserAccount(item))
            }
        }

        if (currentUser?.isSuperuser == true && item.user?.pk != currentUser?.pk) {
            helper.setImageResource(R.id.ivDeleteUserArrow, R.drawable.ic_arrow_right_gray)
            helper.setBackgroundColor(R.id.rlPanelUsersRowMain, getColor(R.color.white))
            helper.getView<SwipeLayout>(R.id.swipe).isSwipeEnabled = true
        } else {
            helper.setImageResource(R.id.ivDeleteUserArrow, R.drawable.ic_lock)
            helper.setBackgroundColor(R.id.rlPanelUsersRowMain, getColor(R.color.gray2))
            helper.getView<SwipeLayout>(R.id.swipe).isSwipeEnabled = false
        }
    }

    private var currentUser: User? = null

    @Subscribe(threadMode = ThreadMode.MAIN_ORDERED)
    fun onData(event: EventReceivedData) {
        if (event.type == DataType.CurrentUser) {
            currentUser = event.data as User
            adapter?.notifyDataSetChanged()
        }
    }
}

