package com.crowcloud.gateway2.fragment

import android.widget.TextView
import androidx.navigation.Navigation
import com.crowcloud.gateway2.App
import com.crowcloud.gateway2.R
import com.crowcloud.gateway.suricataCore.events.*
import com.crowcloud.gateway.suricataCore.fragment.BaseFragment
import com.crowcloud.gateway.suricataCore.login.LoginManager
import com.dd.processbutton.iml.ActionProcessButton
import kotlinx.android.synthetic.main.fragment_login.*
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode

class LoginFragment : BaseFragment() {
    override fun layoutResId(): Int = R.layout.fragment_login
    override fun dataType(): DataType? = null
    override fun showContentOnly(): Boolean  = true

    override fun initView() {
        tvForgotPass.setOnClickListener {
            Navigation.findNavController(it).navigate(R.id.action_loginFragment_to_forgotPasswordFragment)
        }
        tvSignUp.setOnClickListener(Navigation.createNavigateOnClickListener(R.id.action_loginFragment_to_signUpFragment))
        btnlogin.setOnClickListener {
            btnlogin.setMode(ActionProcessButton.Mode.ENDLESS)
            btnlogin.progress = 1
            App.get()?.eventBus?.post(EventTryLogin(etUserAccount.text.toString(), etPasswordAccount.text.toString(), cbRememberPass.isChecked))
        }
    }

    override fun requestData() = postRequestData(DataType.LoginCreds)

    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    fun onData(event: EventReceivedData) {
        when (event.type) {
            DataType.LoginCreds -> {
                if (event.data != null && event.data is LoginManager.LoginCreds) {
                    val creds = event.data as LoginManager.LoginCreds
                    etUserAccount.setText(creds.userName, TextView.BufferType.EDITABLE)
                    etPasswordAccount.setText(creds.password)
                    cbRememberPass.isChecked = true
                }
            }
            else -> return
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onEvent(event: EventLoginDone) {
        btnlogin?.progress = 0
    }
}
