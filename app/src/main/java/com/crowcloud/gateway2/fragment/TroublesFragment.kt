package com.crowcloud.gateway2.fragment

import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.BaseViewHolder
import com.crowcloud.gateway2.R
import com.crowcloud.gateway.suricataCore.components.ComponentData
import com.crowcloud.gateway.suricataCore.events.DataType
import com.crowcloud.gateway.suricataCore.events.EventResetTroubles
import com.crowcloud.gateway.suricataCore.fragment.BaseListFragment
import kotlinx.android.synthetic.main.fragment_troubles.*

class TroublesFragment : BaseListFragment<ComponentData.GeneralTrouble>() {
    override fun dataType(): DataType = DataType.AllTroubles
    override fun layoutResId(): Int = R.layout.fragment_troubles

    override fun initView() {
        super.initView()
        btnReset.setOnClickListener { post(EventResetTroubles()) }
    }

    override fun createAdapter(): BaseQuickAdapter<ComponentData.GeneralTrouble, BaseViewHolder>? =
            object : BaseQuickAdapter<ComponentData.GeneralTrouble, BaseViewHolder>(R.layout.row_troubles) {
                override fun convert(helper: BaseViewHolder?, item: ComponentData.GeneralTrouble) {
                    helper?.setText(R.id.messageTrouble, item.descResId)
                    helper?.setImageResource(R.id.troubles_img_trouble, getImageResource(item.descResId))
                    helper?.setVisible(R.id.tvZoneName, item.device != null)
                    helper?.setText(R.id.tvZoneName, item.device?.name)
                }
            }

    //get icon according to trouble
    private fun getImageResource(field: Int): Int = when (field) {
            R.string.fire_alarm -> R.drawable.ic_troubles_fire
            R.string.panic_alarm -> R.drawable.ic_troubles_panic
            R.string.ethernet_fail -> R.drawable.ic_troubles_ethernet_fail
            R.string.gprs_fail -> R.drawable.ic_troubles_gsm
            R.string.rf_jamming_alarm -> R.drawable.ic_troubles_rf_jamming
            R.string.medical_alarm -> R.drawable.ic_troubles_medical_alarm
            R.string.trouble_low_battery -> R.drawable.ic_troubles_battery_low
            R.string.panel_tamper -> R.drawable.ic_troubles_tamper
            else -> R.drawable.ic_troubles_tamper
    }

}