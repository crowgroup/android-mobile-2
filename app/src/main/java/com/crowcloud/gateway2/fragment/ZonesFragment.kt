package com.crowcloud.gateway2.fragment

import android.widget.Button
import android.widget.TextView
import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.BaseViewHolder
import com.crowcloud.gateway.suricataCore.events.*
import com.crowcloud.gateway.suricataCore.fragment.BaseListFragment
import com.crowcloud.gateway.suricataCore.network.model.response.Area
import com.crowcloud.gateway.suricataCore.network.model.response.Zone
import com.crowcloud.gateway.suricataCore.util.getColor
import com.crowcloud.gateway2.R
import com.crowcloud.gateway2.util.rssiIconResId
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode

class ZonesFragment : BaseListFragment<Zone>() {
    override fun dataType(): DataType = DataType.Zones

    override fun requestData() {
        super.requestData()
        post(EventRequestData(DataType.Areas))
    }

    private val inProgressIds: ArrayList<Int> = ArrayList()
    private var areas: List<Area>? = null

    override fun createAdapter(): BaseQuickAdapter<Zone, in BaseViewHolder>? =
            object : BaseQuickAdapter<Zone, BaseViewHolder>(R.layout.row_zone) {
                override fun convert(helper: BaseViewHolder?, item: Zone) {
                    val areaList :String? = areas?.filter { item.areas?.contains(it.id) ?: false }?.joinToString(separator = ",") { it.name ?: "Area ${it.id}" }
                    helper?.setText(R.id.tvAreasOfZone, areaList ?: getString(R.string.not_belong_area))
                    helper?.setTextColor(R.id.tvAreasOfZone, getColor(if(areaList != null) R.color.my_app_color else R.color.red))
                    helper?.setText(R.id.tvZoneName, item.name)
                    helper?.setText(R.id.btnBypass, if (!item.bypass) R.string.bypass_buttonsTxt else R.string.unbypass)
                    helper?.setText(R.id.tvRssiStatus, if (item.rssi > 0) "${item.rssi}%" else "")
                    helper?.getView<TextView>(R.id.tvRssiStatus)?.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, rssiIconResId(item.rssi))
                    val troubles: String? = item.troublesList.joinToString(separator = ",")
                    helper?.setTextColor(R.id.tvStatus, getColor(when {
                        !item.state -> R.color.green
                        troubles.isNullOrEmpty() -> R.color.gray
                        else -> R.color.red
                    }))
                    helper?.setText(R.id.tvStatus, when {
                        !item.state -> getString(R.string.ready_regular)
                        troubles.isNullOrEmpty() -> getString(R.string.not_ready_regular)
                        else -> troubles
                    })

                    helper?.setVisible(R.id.pbZone, inProgressIds.contains(item.id))

                    helper?.getView<Button>(R.id.btnBypass)?.apply {
                        setOnClickListener {
                            item.let { post(EventBypassRequest(it)) }
                            item.id.let { inProgressIds.add(it) }
                            notifyItemChanged(helper.layoutPosition)
                        }
                        isEnabled = !inProgressIds.contains(item.id)
                    }
                }
            }

    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    fun onData(event: EventReceivedData){
        if(event.type == DataType.Areas){
            areas = event.data as List<Area>
            adapter?.notifyDataSetChanged()
        }
    }

    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    fun onData(event: EventBypassZoneDone){
        inProgressIds.remove(event.zone.id)
        if(event.error == null){
            val idx = adapter?.data?.indexOfFirst { it.id == event.zone.id } ?: 0
            adapter?.remove(idx)
            adapter?.addData(idx, event.zone)
            adapter?.notifyItemChanged(idx)
        }
    }
}