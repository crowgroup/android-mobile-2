package com.crowcloud.gateway2.fragment

import android.widget.TextView
import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.BaseViewHolder
import com.crowcloud.gateway.suricataCore.events.DataType
import com.crowcloud.gateway.suricataCore.fragment.BaseListFragment
import com.crowcloud.gateway.suricataCore.network.model.response.BaseDevice
import com.crowcloud.gateway.suricataCore.util.getColor
import com.crowcloud.gateway2.R
import com.crowcloud.gateway2.util.rssiIconResId


class KeypadsFragment : BaseListFragment<BaseDevice>() {
    override fun dataType(): DataType = DataType.Keypads

    override fun createAdapter(): BaseQuickAdapter<BaseDevice, in BaseViewHolder>? =
            object : BaseQuickAdapter<BaseDevice, BaseViewHolder>(R.layout.row_keypad) {
                override fun convert(helper: BaseViewHolder?, item: BaseDevice?) {
                    helper?.setText(R.id.tvKeypadStatus, item?.name)

                    helper?.setText(R.id.tvRssiStatus, if(item?.rssi ?: 0 > 0)"${item?.rssi}%" else "")
                    helper?.getView<TextView>(R.id.tvRssiStatus)?.setCompoundDrawablesWithIntrinsicBounds(0,0,0, rssiIconResId(item?.rssi ?: 0))
                    val troubles : String? = item?.troublesList?.joinToString(separator = ",")
                    helper?.setTextColor(R.id.tvKeypadStatus, getColor( if(troubles.isNullOrEmpty()) R.color.green else R.color.red))
                    helper?.setText(R.id.tvKeypadStatus, if(troubles.isNullOrEmpty()) getString(R.string.ready_regular) else troubles )
                }
            }
}