package com.crowcloud.gateway2.fragment

import android.os.Bundle
import android.view.View
import android.widget.TextView
import com.chad.library.adapter.base.BaseViewHolder
import com.crowcloud.gateway.suricataCore.events.DataType
import com.crowcloud.gateway.suricataCore.extensions.visible
import com.crowcloud.gateway.suricataCore.fragment.BaseListFragment
import com.crowcloud.gateway.suricataCore.network.model.response.Measurement
import com.crowcloud.gateway.suricataCore.network.model.response.MeasuresByZone
import com.crowcloud.gateway2.R

class MeasuresFragment : BaseListFragment<MeasuresByZone>() {
    override fun dataType(): DataType = DataType.Measurements
    override fun itemLayoutResId() = R.layout.row_latest_measurement

    override fun convertItemToView(helper: BaseViewHolder, item: MeasuresByZone) {
        helper.setText(R.id.tvLastMeasureTitle, item.name)

        item.values?.forEach {
            helper.getView<View>(R.id.tvTemperature).visible(it?.isTemperature() == true)
            helper.setText(R.id.tvTemperature, "${(it?.temperature ?: 0F) / 100}℃")

            helper.getView<View>(R.id.tvHumidity).visible(it?.isHumidity() == true)
            helper.setText(R.id.tvHumidity, "${(it?.humidity ?: 0F) / 100}%RH")

            helper.getView<View>(R.id.tvAirPressure).visible(it?.isAirPressure() == true)
            helper.setText(R.id.tvAirPressure, "${(it?.airPressure ?: 0F) / 100}hPa")
        }

        helper.getView<TextView>(R.id.tvTemperature).setOnClickListener {
            navController().navigate(R.id.measurementStatsFragment, Bundle().apply {
                putSerializable(ARG_MEASUREMENT, item.values?.find { it?.isTemperature() == true })
                putSerializable(ARG_TYPE, Measurement.MeasureType.Temperature)
            })
        }
        helper.getView<TextView>(R.id.tvHumidity).setOnClickListener {
            navController().navigate(R.id.measurementStatsFragment, Bundle().apply {
                putSerializable(ARG_MEASUREMENT, item.values?.find { it?.isHumidity() == true })
                putSerializable(ARG_TYPE, Measurement.MeasureType.Humidity)
            })
        }
        helper.getView<TextView>(R.id.tvAirPressure).setOnClickListener {
            navController().navigate(R.id.measurementStatsFragment, Bundle().apply {
                putSerializable(ARG_MEASUREMENT, item.values?.find { it?.isAirPressure() == true })
                putSerializable(ARG_TYPE, Measurement.MeasureType.AirPressure)
            })
        }
    }
}