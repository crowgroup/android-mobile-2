package com.crowcloud.gateway2.fragment

import com.crowcloud.gateway2.R
import com.crowcloud.gateway.suricataCore.events.DataType.*
import com.crowcloud.gateway.suricataCore.events.DataType
import com.crowcloud.gateway.suricataCore.events.EventReceivedData
import com.crowcloud.gateway.suricataCore.fragment.BaseFragment
import com.crowcloud.gateway.suricataCore.network.model.response.Panel
import kotlinx.android.synthetic.main.fragment_panel_info.*
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode

class PanelInfoFragment : BaseFragment() {
    override fun dataType(): DataType?  = RadioInfo

    override fun requestData() = postRequestData(CurrentPanel,RadioInfo, EthernetInfo, WifiInfo, GsmInfo)

    override fun layoutResId(): Int = R.layout.fragment_panel_info

    override fun initView() { }

    @Subscribe(threadMode = ThreadMode.MAIN_ORDERED)
    fun onData(event:EventReceivedData){
        when(event.type){
            CurrentPanel -> if(event.data is Panel) {
                val data = event.data as Panel
                tvPanelVerTitle.text = getString(R.string.panel_ver, data.version)
                tvMacTitle.text = getString(R.string.mac, data.mac)
            }
            RadioInfo -> if(event.data is com.crowcloud.gateway.suricataCore.network.model.response.RadioInfo){
                val data = event.data as com.crowcloud.gateway.suricataCore.network.model.response.RadioInfo
                tvHardwareVer.text = getString(R.string.hw_ver, data.hardwareVersion)
                tvSoftwareVer.text = getString(R.string.sw_ver, data.softwareVersion)
                tvFrequency.text = getString(R.string.frequency, data.freq.toString())
            }
            EthernetInfo -> if(event.data is com.crowcloud.gateway.suricataCore.network.model.response.EthernetInfo){
                val data = event.data as com.crowcloud.gateway.suricataCore.network.model.response.EthernetInfo
                tvEthernetIp.text = getString(R.string.ip_address, data.ip)
                tvEthernetMask.text = getString(R.string.mask_ip, data.mask)
                tvEthernetGateway.text = getString(R.string.panel_gateway, data.gateway)
                if(data.ip != "0.0.0.0"){
                    tvConnectedUsing.text = getString(R.string.connected_using,getString(R.string.ethernet_title))
                }
            }
            GsmInfo -> if(event.data is com.crowcloud.gateway.suricataCore.network.model.response.GsmInfo){
                val data = event.data as com.crowcloud.gateway.suricataCore.network.model.response.GsmInfo
                tvIpGsm.text = getString(R.string.ip_address, data.ip)
                tvMaskGsm.text = getString(R.string.mask_ip, data.mask)
                tvDnsGsm.text = getString(R.string.dns, data.dns)
                tvRssiGsm.text = getString(R.string.rssi_level,data.rssi.toString())
                tvGatewayGsm.text = getString(R.string.panel_gateway,data.gateway)
                tvIMEIGsm.text = getString(R.string.imei,data.imei)
                tvProviderGsm.text = getString(R.string.provider,data.provider)
                tvStatusDescGsm.text = getString(R.string.status_desc,data.statusDesc)

                if(data.ip != "0.0.0.0"){
                    tvConnectedUsing.text = getString(R.string.connected_using,getString(R.string.gsm_title1))
                }
            }
            WifiInfo -> if(event.data is com.crowcloud.gateway.suricataCore.network.model.response.WifiInfo){
                val data = event.data as com.crowcloud.gateway.suricataCore.network.model.response.WifiInfo
                tvIpWifi.text = getString(R.string.ip_address, data.ip)
                tvMaskWifi.text = getString(R.string.mask_ip, data.mask)
                tvDnsWifi.text = getString(R.string.dns, data.dns)
                tvSsidWifi.text = getString(R.string.ssid,data.ssid)
                tvRssiWifi.text = getString(R.string.rssi_level,data.rssi.toString())
                tvGatewayWifi.text = getString(R.string.panel_gateway,data.gateway)

                if(data.ip != "0.0.0.0"){
                    tvConnectedUsing.text = getString(R.string.connected_using,getString(R.string.wifi_title))
                }
            }
            else->{}
        }
    }
}