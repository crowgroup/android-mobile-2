package com.crowcloud.gateway2.fragment

import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.BaseViewHolder
import com.crowcloud.gateway2.R
import com.crowcloud.gateway.suricataCore.events.DataType
import com.crowcloud.gateway.suricataCore.events.EventShowPictures
import com.crowcloud.gateway.suricataCore.fragment.BaseListFragment
import com.crowcloud.gateway.suricataCore.network.model.response.Event
import com.crowcloud.gateway.suricataCore.network.model.response.Event.AlarmType.*
import com.crowcloud.gateway.suricataCore.util.getLocale
import java.text.SimpleDateFormat

class EventsFragment : BaseListFragment<Event>() {
    override fun dataType(): DataType = DataType.Events
    override fun pageSize(): Int = 12
    override fun createAdapter(): BaseQuickAdapter<Event, in BaseViewHolder>? =
            object : BaseQuickAdapter<Event, BaseViewHolder>(R.layout.row_event) {
                val dateFormat = SimpleDateFormat("yyyy,EEE MMM dd  HH:mm:ss", getLocale(context))
                override fun convert(helper: BaseViewHolder?, item: Event) {
                    helper?.itemView?.setOnClickListener { post(EventShowPictures(getItem(helper.layoutPosition))) }
                    helper?.setText(R.id.tvDate, dateFormat.format(item.created))
                    helper?.setText(R.id.title, item.text)
                    helper?.setVisible(R.id.icon_photo, item.pictures?.isNotEmpty() == true)
                    helper?.setImageResource(R.id.symbol, when (item.cid) {
                        EVENT_PANIC_ALARM,
                        EVENT_MEDICAL_ALARM,
                        EVENT_FIRE_ALARM,
                        EVENT_ALARM -> if (item.pictures?.isNotEmpty() == true) R.drawable.ic_events_camera_red else R.drawable.ic_event_alarm
                        EVENT_TAKE_PIC -> R.drawable.ic_events_camera_blue
                        EVENT_BATTERY_LOW -> R.drawable.ic_events_low_battery
                        EVENT_ARMED -> R.drawable.ic_event_arm
                        EVENT_DISARMED -> R.drawable.ic_event_disarm
                        EVENT_STAYARM -> R.drawable.ic_event_stay
                        else -> R.drawable.ic_events_default
                    })
                }
            }
}