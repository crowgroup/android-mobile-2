package com.crowcloud.gateway2.fragment

import android.text.TextUtils
import com.crowcloud.gateway.suricataCore.events.DataType
import com.crowcloud.gateway.suricataCore.events.DataType.CurrentUser
import com.crowcloud.gateway.suricataCore.events.EventReceivedData
import com.crowcloud.gateway.suricataCore.events.EventUpdateUserData
import com.crowcloud.gateway.suricataCore.fragment.BaseFragment
import com.crowcloud.gateway.suricataCore.network.model.response.User
import com.crowcloud.gateway.suricataCore.util.simple_callback.SimpleTextWatcher
import com.crowcloud.gateway2.R
import kotlinx.android.synthetic.main.fragment_my_profile.*
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode

class EditProfileFragment : BaseFragment() {
    override fun dataType(): DataType? = CurrentUser

    override fun requestData() = postRequestData(CurrentUser)

    override fun layoutResId(): Int = R.layout.fragment_my_profile

    override fun initView() {
        firstNameEt.addTextChangedListener(object : SimpleTextWatcher() {
            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                firstWrapper.error = ""
                firstWrapper.isErrorEnabled = false
            }
        })

        lastNameEt.addTextChangedListener(object : SimpleTextWatcher() {
            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                lastWrapper.error = ""
                lastWrapper.isErrorEnabled = false
            }
        })

        changePasswordButton.setOnClickListener { navController().navigate(R.id.changePasswordFragment) }

        updateButton.setOnClickListener { trySubmit() }

    }

    private fun trySubmit() {
        val firstName = firstNameEt.text.toString()
        val lastName = lastNameEt.text.toString()

        var isValid = true
        if (TextUtils.isEmpty(firstName)) {
            firstWrapper.isErrorEnabled = true
            firstWrapper.error = getString(R.string.emptyField_error)
            isValid = false
        }

        if (TextUtils.isEmpty(lastName)) {
            lastWrapper.isErrorEnabled = true
            lastWrapper.error = getString(R.string.emptyField_error)
            isValid = false
        }

        if (isValid) {
            post(EventUpdateUserData(firstName, lastName))
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN_ORDERED)
    fun onData(event: EventReceivedData) {
        if (event.type == CurrentUser) {
            val user = event.data as User
            firstNameEt.setText(user.firstName)
            lastNameEt.setText(user.lastName)
        }
    }
}