package com.crowcloud.gateway2.fragment

import android.support.v4.widget.SwipeRefreshLayout
import com.crowcloud.gateway2.R
import com.crowcloud.gateway2.adapter.AreasPagerAdapter
import com.crowcloud.gateway.suricataCore.events.DataType
import com.crowcloud.gateway.suricataCore.events.EventReceivedData
import com.crowcloud.gateway.suricataCore.events.EventRequestData
import com.crowcloud.gateway.suricataCore.fragment.BaseFragment
import com.crowcloud.gateway.suricataCore.network.model.response.Area
import kotlinx.android.synthetic.main.fragment_control.*
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode

class ControlFragment : BaseFragment() {

    private var adapter: AreasPagerAdapter? = null

    override fun layoutResId(): Int = R.layout.fragment_control
    override fun dataType(): DataType = DataType.Areas
    override fun swipeRefresh(): SwipeRefreshLayout?  = null

    override fun requestData() {
        post(EventRequestData(DataType.Areas))
        post(EventRequestData(DataType.Troubles))
        post(EventRequestData(DataType.Measurements))
    }

    override fun initView() {
        pcZoneMeasure.isClickable = false
        tlAreaNames.setupWithViewPager(pager)
        adapter = AreasPagerAdapter(childFragmentManager, ArrayList())
    }

    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    fun onData(event:EventReceivedData){
        if(event.type == DataType.Areas && event.data is List<*>) {
            if(adapter == null){
                adapter = AreasPagerAdapter(childFragmentManager, event.data as List<Area>)
                pager.adapter = adapter
            } else {
                adapter?.areas = event.data as List<Area>
                if(pager.adapter == null){
                    pager.adapter = adapter
                }
            }
        }
    }

}
